<?php

/* base.html.twig */
class __TwigTemplate_e1724d6cbc9d782bd495b043d021ce99da8d61e4157db2e9395d006b36e2f764 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33204c01c5196fcad0524be6274524dd5efa2664f94e876d6601b1b51ae355df = $this->env->getExtension("native_profiler");
        $__internal_33204c01c5196fcad0524be6274524dd5efa2664f94e876d6601b1b51ae355df->enter($__internal_33204c01c5196fcad0524be6274524dd5efa2664f94e876d6601b1b51ae355df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_33204c01c5196fcad0524be6274524dd5efa2664f94e876d6601b1b51ae355df->leave($__internal_33204c01c5196fcad0524be6274524dd5efa2664f94e876d6601b1b51ae355df_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_2ee5ec02a4cb434098b9d1685411bcc037b6d598ee7d4d9d08f8611444b447cf = $this->env->getExtension("native_profiler");
        $__internal_2ee5ec02a4cb434098b9d1685411bcc037b6d598ee7d4d9d08f8611444b447cf->enter($__internal_2ee5ec02a4cb434098b9d1685411bcc037b6d598ee7d4d9d08f8611444b447cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_2ee5ec02a4cb434098b9d1685411bcc037b6d598ee7d4d9d08f8611444b447cf->leave($__internal_2ee5ec02a4cb434098b9d1685411bcc037b6d598ee7d4d9d08f8611444b447cf_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_87c3574c6b9c9555079ef7bc22c80ef8ebd8e0f8e59cf17393d090de65543a77 = $this->env->getExtension("native_profiler");
        $__internal_87c3574c6b9c9555079ef7bc22c80ef8ebd8e0f8e59cf17393d090de65543a77->enter($__internal_87c3574c6b9c9555079ef7bc22c80ef8ebd8e0f8e59cf17393d090de65543a77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_87c3574c6b9c9555079ef7bc22c80ef8ebd8e0f8e59cf17393d090de65543a77->leave($__internal_87c3574c6b9c9555079ef7bc22c80ef8ebd8e0f8e59cf17393d090de65543a77_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_0d75c4bd3d99820b2ea31dba9da579e51a73ca82308b73a6fe35f98fb8d68e3d = $this->env->getExtension("native_profiler");
        $__internal_0d75c4bd3d99820b2ea31dba9da579e51a73ca82308b73a6fe35f98fb8d68e3d->enter($__internal_0d75c4bd3d99820b2ea31dba9da579e51a73ca82308b73a6fe35f98fb8d68e3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_0d75c4bd3d99820b2ea31dba9da579e51a73ca82308b73a6fe35f98fb8d68e3d->leave($__internal_0d75c4bd3d99820b2ea31dba9da579e51a73ca82308b73a6fe35f98fb8d68e3d_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_84b48d46ccae8ec02cf33ce4030bcca4eb3ae1354c22c61d2ef4558959acf74e = $this->env->getExtension("native_profiler");
        $__internal_84b48d46ccae8ec02cf33ce4030bcca4eb3ae1354c22c61d2ef4558959acf74e->enter($__internal_84b48d46ccae8ec02cf33ce4030bcca4eb3ae1354c22c61d2ef4558959acf74e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_84b48d46ccae8ec02cf33ce4030bcca4eb3ae1354c22c61d2ef4558959acf74e->leave($__internal_84b48d46ccae8ec02cf33ce4030bcca4eb3ae1354c22c61d2ef4558959acf74e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
