<?php

/* AppBundle:Usuarios:new.html.twig */
class __TwigTemplate_a47d998bc325c07d248655e27bbe0ddea3c56008f53667ab3c9798f0194c0e5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Usuarios:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4da6b63fa191faae811bc85e942aa436ca70dbe16b69585a40f148a0c65e94ab = $this->env->getExtension("native_profiler");
        $__internal_4da6b63fa191faae811bc85e942aa436ca70dbe16b69585a40f148a0c65e94ab->enter($__internal_4da6b63fa191faae811bc85e942aa436ca70dbe16b69585a40f148a0c65e94ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Usuarios:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4da6b63fa191faae811bc85e942aa436ca70dbe16b69585a40f148a0c65e94ab->leave($__internal_4da6b63fa191faae811bc85e942aa436ca70dbe16b69585a40f148a0c65e94ab_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2bfb39d715be1ae1ba26707f0b2bf95928a0a8bb99f4b94715093c8027774bcf = $this->env->getExtension("native_profiler");
        $__internal_2bfb39d715be1ae1ba26707f0b2bf95928a0a8bb99f4b94715093c8027774bcf->enter($__internal_2bfb39d715be1ae1ba26707f0b2bf95928a0a8bb99f4b94715093c8027774bcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Usuarios creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("usuarios");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
        
        $__internal_2bfb39d715be1ae1ba26707f0b2bf95928a0a8bb99f4b94715093c8027774bcf->leave($__internal_2bfb39d715be1ae1ba26707f0b2bf95928a0a8bb99f4b94715093c8027774bcf_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Usuarios:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Usuarios creation</h1>*/
/* */
/*     {{ form(form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('usuarios') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
