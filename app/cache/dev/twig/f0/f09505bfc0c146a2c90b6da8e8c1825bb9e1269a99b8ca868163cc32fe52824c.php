<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_7ffd241ddfd8f0845fb9c4f0904fe12f7b0e9477c4ffc4042370d649f639c71c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b6a966398c19abd6e0d44d76e17bbe8b591caa147a98024291b9cb175d7da5d = $this->env->getExtension("native_profiler");
        $__internal_0b6a966398c19abd6e0d44d76e17bbe8b591caa147a98024291b9cb175d7da5d->enter($__internal_0b6a966398c19abd6e0d44d76e17bbe8b591caa147a98024291b9cb175d7da5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b6a966398c19abd6e0d44d76e17bbe8b591caa147a98024291b9cb175d7da5d->leave($__internal_0b6a966398c19abd6e0d44d76e17bbe8b591caa147a98024291b9cb175d7da5d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a06456596bafe089d538bfc4504688ca61b743e8e26859937a9be8149eb1d3c5 = $this->env->getExtension("native_profiler");
        $__internal_a06456596bafe089d538bfc4504688ca61b743e8e26859937a9be8149eb1d3c5->enter($__internal_a06456596bafe089d538bfc4504688ca61b743e8e26859937a9be8149eb1d3c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a06456596bafe089d538bfc4504688ca61b743e8e26859937a9be8149eb1d3c5->leave($__internal_a06456596bafe089d538bfc4504688ca61b743e8e26859937a9be8149eb1d3c5_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_a95cff5922ac4ce40f1df153e5533e8f09c9a67e9078297bb81cf2abec8b7abf = $this->env->getExtension("native_profiler");
        $__internal_a95cff5922ac4ce40f1df153e5533e8f09c9a67e9078297bb81cf2abec8b7abf->enter($__internal_a95cff5922ac4ce40f1df153e5533e8f09c9a67e9078297bb81cf2abec8b7abf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_a95cff5922ac4ce40f1df153e5533e8f09c9a67e9078297bb81cf2abec8b7abf->leave($__internal_a95cff5922ac4ce40f1df153e5533e8f09c9a67e9078297bb81cf2abec8b7abf_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_5f9b57049f047698abc075afcd7bfbca500e590fce4d9a971452fb4f793d2ccd = $this->env->getExtension("native_profiler");
        $__internal_5f9b57049f047698abc075afcd7bfbca500e590fce4d9a971452fb4f793d2ccd->enter($__internal_5f9b57049f047698abc075afcd7bfbca500e590fce4d9a971452fb4f793d2ccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_5f9b57049f047698abc075afcd7bfbca500e590fce4d9a971452fb4f793d2ccd->leave($__internal_5f9b57049f047698abc075afcd7bfbca500e590fce4d9a971452fb4f793d2ccd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
